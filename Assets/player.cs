﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Text;
using System.Linq;

public class player : MonoBehaviour
{

    private Animator animator;                                                                  // Animator
    private String[,] dirMap;                                                                   // stroes the arrow map
    private int[] currentCell;                                                                  // stores current location of player
    private bool isMoving;                                                                      // boolean for checking if corouting running at the moment
    private GameManager manager;

    private List<int[]> backSequence;
    private List<int[]> frontSequence;
    private List<int[]> playerPath;

    private int matchingCellIndex;
    bool reset;
    bool inSequence;
    bool inBackSequence;
    bool inFrontSequence;
    int resumeFrom;

    int numberOfRepeats;
    int stepsThroughSequence;


    Coroutine co;

    public void Start()
    {
        manager = GameObject.Find("GameManager").GetComponent<GameManager>();
        //dirMap = null;
        animator = GetComponent<Animator>();
        currentCell = GameObject.Find("Maker").GetComponent<create>().StartNode;
        isMoving = false;

        backSequence = new List<int[]>();
        frontSequence = new List<int[]>();
        playerPath = new List<int[]>();

        inBackSequence = false;
        inFrontSequence = false;

        transform.position = new Vector3(currentCell[0] * 2.5f + 1.25f, currentCell[1] * 2.5f + 1.25f, 0);
    }

    private void Update()
    {

    }

    void FixedUpdate()
    {

        float Horiz = Input.GetAxis("Horizontal");
        float Vert = Input.GetAxis("Vertical");


        if (Horiz > 0.1f)
        {
            MovePlayer(direction.RIGHT);
        }

        if (Horiz < -0.1f)
        {
            MovePlayer(direction.LEFT);
        }

        if (Vert > 0.1f)
        {
            MovePlayer(direction.UP);
        }

        if (Vert < -0.1f)
        {
            MovePlayer(direction.DOWN);
        }

    }

    private void OnCollisionEnter(Collision other)
    {
        if (other.collider.name == "Enemy")
            manager.ResetLevel();
    }

    public enum direction
    {
        UP,
        RIGHT,
        DOWN,
        LEFT,
    }

    public String[,] DirMap
    {
        get
        {
            return dirMap;
        }
        set
        {
            dirMap = value;
        }
    }

    public List<int[]> PlayerPath
    {
        get
        {
            return playerPath;
        }
        set
        {
            playerPath = value;
        }
    }

    public int[] CurrentCell
    {
        get
        {
            return currentCell;
        }
        set
        {
            currentCell = value;
        }
    }

    /// <summary>
    /// Function checks map restrictions for movement
    /// and checks for current movement
    /// </summary>
    /// <param name="dir"></param>
    private void MovePlayer(direction dir)
    {
        //if (!isMoving)
        //{                  

        switch (dir)
        {
            case direction.UP:

                if (CanMove('0') && !isMoving)
                {
                    if (!RequiresReset())
                    {
                        transform.rotation = Quaternion.Euler(-90, 90, -90);
                        co = StartCoroutine(Movement(new Vector3(currentCell[0] * 2.5f + 1.25f, currentCell[1] * 2.5f + 1.25f + 2.5f, 0)));
                    }
                }
                break;

            case direction.RIGHT:

                if (CanMove('1') && !isMoving)
                {
                    if (!RequiresReset())
                    {
                        transform.rotation = Quaternion.Euler(0, 90, -90);
                        co = StartCoroutine(Movement(new Vector3(currentCell[0] * 2.5f + 1.25f + 2.5f, currentCell[1] * 2.5f + 1.25f, 0)));
                    }
                }
                break;

            case direction.DOWN:

                if (CanMove('2') && !isMoving)
                {
                    if (!RequiresReset())
                    {
                        transform.rotation = Quaternion.Euler(90, 90, -90);
                        co = StartCoroutine(Movement(new Vector3(currentCell[0] * 2.5f + 1.25f, currentCell[1] * 2.5f + 1.25f - 2.5f, 0)));
                    }
                }
                break;

            case direction.LEFT:

                if (CanMove('3') && !isMoving)
                {
                    if (!RequiresReset())
                    {
                        transform.rotation = Quaternion.Euler(180, 90, -90);
                        co = StartCoroutine(Movement(new Vector3(currentCell[0] * 2.5f + 1.25f - 2.5f, currentCell[1] * 2.5f + 1.25f, 0)));
                    }
                }
                break;

            default:
                break;

        }
    }
    //}

    private bool RequiresReset()
    {
        if (CheckForRepeats())
        {
            reset = true;
        }
        if (reset)
        {
            ResetPlayer();
            reset = false;
            return true;
        }
        return false;
    }
    private bool CheckForRepeats()
    {
        if (IsVisited() && IsRepeating() || inSequence && IsRepeating())
        {
            stepsThroughSequence++;
            if (inBackSequence && stepsThroughSequence >= backSequence.Count)
            {
                numberOfRepeats++;
                stepsThroughSequence = 0;
            }
            if (inFrontSequence && stepsThroughSequence >= frontSequence.Count)
            {
                numberOfRepeats++;
                stepsThroughSequence = 0;
            }
        }
        if (numberOfRepeats > 2)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool IsVisited()
    {
        int previousCellIndex = playerPath.Count - 2;

        if (!inSequence)
        {
            ResetSequences();
        }

        if ((IsNullOrEmpty(backSequence) && IsNullOrEmpty(frontSequence)) || inSequence)
        {
            for (int i = previousCellIndex; i > 0; i--)
            {
                if (currentCell[0] == playerPath[i][0] && currentCell[1] == playerPath[i][1])
                {
                    matchingCellIndex = i;
                    inSequence = true;
                    return true;
                }
            }
        }
        ResetSequences();
        return false;
    }
    private bool IsRepeating()
    {
        if (playerPath.Count > 5)
        {
            if (IsNullOrEmpty(backSequence) && IsNullOrEmpty(frontSequence))
            {
                GetSequence();
            }
            for (int i = resumeFrom; i <= backSequence.Count - 1; i++)
            {
                if (currentCell[0] == backSequence[i][0] || currentCell[1] == backSequence[i][1])
                {
                    inBackSequence = true;
                    resumeFrom = i + 1;
                    if (resumeFrom >= backSequence.Count - 1)
                        resumeFrom = 0;
                    return true;
                }
            }
            for (int i = resumeFrom; i <= frontSequence.Count - 1; i++)
            {
                if (currentCell[0] == frontSequence[i][0] && currentCell[1] == frontSequence[i][1])
                {
                    inFrontSequence = true;
                    resumeFrom = i + 1;
                    if (resumeFrom >= backSequence.Count - 1)
                        resumeFrom = 0;
                    return true;
                }

            }
        }
        ResetSequences();
        return false;
    }

    private void GetSequence()
    {

        if (playerPath.Count > 7)
        {
            for (int i = 0; i < 5; i++)
            {
                WriteTheSequence(i);
            }
        }
        else
        {
            for (int i = 0; i < matchingCellIndex; i++)
            {
                WriteTheSequence(i);
            }
        }
    }



    private void WriteTheSequence(int i)
    {
        var backSequenceCharacter = playerPath[matchingCellIndex - i];
        backSequence.Add(backSequenceCharacter);

        if (playerPath.Count >= matchingCellIndex + i + 1)
        {
            var frontSequenceCharacter = playerPath[matchingCellIndex + i];
            frontSequence.Add(frontSequenceCharacter);
        }
    }

    private void ResetSequences()
    {
        backSequence.Clear();
        frontSequence.Clear();
        inSequence = false;
        inFrontSequence = false;
        inBackSequence = false;
        resumeFrom = 0;
        numberOfRepeats = 0;
    }

    public void ResetPlayer()
    {
        ResetSequences();
        playerPath.Clear();
        //StartCoroutine(Pause(1));
        StopCoroutine(co);
        isMoving = false;
        animator.SetBool("Walk", false);
        currentCell = GameObject.Find("Maker").GetComponent<create>().StartNode;
        transform.position = new Vector3(currentCell[0] * 2.5f + 1.25f, currentCell[1] * 2.5f + 1.25f, 0);

    }

    IEnumerator Movement(Vector3 target)
    {
        isMoving = true;
        playerPath.Add(currentCell.ToArray());

        animator.SetBool("Walk", true);

        while (transform.position != target)
        {
            transform.position = Vector3.MoveTowards(transform.position, target, 0.1f);
            yield return new WaitForSeconds(0.01f);
        }

        currentCell[0] = (int)((target.x - 1.25f) / 2.5f);
        currentCell[1] = (int)((target.y - 1.25f) / 2.5f);

        manager.PlayerPos = currentCell;

        animator.SetBool("Walk", false);
        isMoving = false;

        yield return null;

    }
    IEnumerator Pause(int p)
    {
        Time.timeScale = 0.0f;
        float pauseEndTime = Time.realtimeSinceStartup + p;
        while (Time.realtimeSinceStartup < pauseEndTime)
        {
            yield return 0;
        }
        Time.timeScale = 1;
    }

    /// <summary>
    /// Function check allowed directions for player in current cell.
    /// true - movement is allowed,
    /// false - movement is forbidden.
    /// </summary>
    /// <param name="turn"></param>
    /// <returns></returns>
    private bool CanMove(char turn)
    {
        foreach (char arrow in dirMap[currentCell[0], currentCell[1]])
        {
            if ((turn).Equals(arrow))
            {
                return true;
            }
        }

        return false;
    }

    public bool IsNullOrEmpty(List<int[]> list)
    {
        return list == null || list.Count == 0 ? true : false;
    }

    public void RestartScript()
    {
        Start();
    }
}
