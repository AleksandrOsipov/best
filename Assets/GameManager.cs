﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{

    private string currentScene;
    private create create;
    private enemy enemy;
    private player player;
    private Light playerLight;
    private int playerHealth;
    private int[] playerPos;
    private int[] finishPos;
    Animator animator;
    Animator HUDanimator;
    

    void Start()
    {
        currentScene = "Level1";


        create = GameObject.Find("Maker").GetComponent<create>();
        enemy = GameObject.FindGameObjectWithTag("Enemy").GetComponent<enemy>();
        player = GameObject.Find("player").GetComponent<player>();
        playerLight = GameObject.Find("player/PlayerLight").GetComponent<Light>();
        animator = GameObject.Find("player").GetComponent<Animator>();
        HUDanimator = GameObject.Find("HUD").GetComponent<Animator>();


        
        playerHealth = 3;
        playerPos = player.CurrentCell;

        finishPos = create.FinishNode;

        // deactivate the enemy for the 1 level
        enemy.gameObject.SetActive(false);
        playerLight.enabled = false;
        
    }

    void Update()
    {

    }

    public string CurrentScene
    {
        get
        {
            return currentScene;
        }
        set
        {
            currentScene = value;
        }
    }
       


    public int[] PlayerPos
    {
        get
        {
            return playerPos;
        }
        set
        {
            playerPos = value;
            CheckPlayer();
        }
    }


    private void CheckPlayer()
    {
        finishPos = create.FinishNode;

        if (playerPos[0] == finishPos[0] &&
            playerPos[1] == finishPos[1] &&
            playerHealth > 0)
        {
            animator.SetBool("Jump", true);
            StartCoroutine(Jump());
            
            
        }
    }

    public void LoadLevel()
    {
        switch (currentScene)
        {
            case "Level1":
                currentScene = "Level2";
                //RenderSettings.ambientLight = Color.black;
                playerLight.enabled = true;
                CreateGame(5, 5, finishPos);

                break;


            case "Level2":
                currentScene = "Level3";
                enemy.gameObject.SetActive(true);
                CreateGame(10, 10, finishPos);
                break;

            case "Level3":
                HUDanimator.SetBool("Win", true);
                break;

            case "loser":
                HUDanimator.SetBool("GameOver", true);
                break;

            default:
                break;
        }
    }


    public void ResetLevel()
    {
        player.ResetPlayer();
        enemy.ResetEnemy();

        //playerHealth--;
        CheckHealth();
    }


    private void CheckHealth()
    {
        if (playerHealth <= 0)
        {
            currentScene = "loser";
            LoadLevel();
        }
    }


    private void CreateGame(int width, int height, int[] start_point)
    {
        create.RestartScript(width, height, start_point);
        player.RestartScript();
        enemy.RestartScript();
    }

    IEnumerator Jump()
    {
        yield return new WaitForSeconds(1);
        animator.SetBool("Jump", false);
        LoadLevel();
    }


}