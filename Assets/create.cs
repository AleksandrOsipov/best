﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class create : MonoBehaviour
{

    public int width;                                                           // width of map
    public int height;                                                          // height of map
   // public int number_of_steps;                                                 // can be implemented as max number of steps for making path
    public GameObject prefab;    
    public GameObject start;                                                    // start node prefab
    public GameObject finish;                                                   // finish node prefab
    public GameObject path_direction;                                           // arrows on map to show path
    public Camera main_camera;                                                  // main camera

    private GameObject tileHolder;
    private GameObject arrowHolder;
    private player player;
    private int[] start_point;                                                  // to set the start point
    private int number_of_calls_for_random_arrows;                              // how many times SetExtraDirections() function will be called
    private GameObject[,] map;                                                  // store all map nodes
    private String[,] dirMap;                                                       // store all arrows
	private List<String> path = new List<String>();                              // store right path from start to finish
    private int[] currentNode;                                                  // store current node

    private bool stuck;                                                         // true - stuck in making path
    private int countTurn = 0;                                                  // counts how many times ChooseNext() failed and returned to choose another direction, used for "stuck" variable
    private String last;                                                        // represents finish coordinates 

    private void OnEnable()
    {
        width = 3;
        height = 3;
        start_point = new int[2] { 0, 0 };
        number_of_calls_for_random_arrows = 3;

        tileHolder = GameObject.Find("TileHolder");
        arrowHolder = GameObject.Find("ArrowHolder");
        player = GameObject.Find("player").GetComponent<player>();
        prefab = GameObject.FindGameObjectWithTag("room");
        start = GameObject.FindGameObjectWithTag("start");
        finish = GameObject.FindGameObjectWithTag("finish");
        path_direction = GameObject.FindGameObjectWithTag("arrow");
    }

    void Start()
    {
        prefab.SetActive(true);
        start.SetActive(true);
        finish.SetActive(true);
        path_direction.SetActive(true);

        map = new GameObject[width, height];
        dirMap = new String[width, height];
        currentNode = new int[2] { start_point[0], start_point[1] };

        // Creating map and path
        MakeMap();
        while (path.Count < (width * height / 5))
        {
            path.Clear();
            path.Add(start_point[0] + "" + start_point[1]);
            stuck = false;
            MakePath();
        }
        SetStartFinish();

        for (int i = 0; i < number_of_calls_for_random_arrows; i++)
        {
            SetExtraDirections();
        }

        main_camera.transform.position = new Vector3(2.5f * width / 2, 2.5f * height / 2, -(height + width) * 1.2f);
        SortArray();

        player.DirMap = dirMap;

        Wait12();

        // Disabling prefabs
        prefab.SetActive(false);
        start.SetActive(false);
        finish.SetActive(false);
        path_direction.SetActive(false);
    }

   
    

    void Update()
    {

    }

    public enum direction
    {
        UP,
        RIGHT,
        DOWN,
        LEFT,
    }

    public int[] StartNode
    {
        get
        {
            return new int[2]  { start_point[0], start_point[1] };
        }
                
    }

    public int[] FinishNode
    {
        get
        {
            return new int[2] { last[0] - '0', last[1] - '0' };
        }
    }

    /// <summary>
    /// Function creates the map using width and height from user.
    /// Each tile is child of tileHolder
    /// </summary>
    private void MakeMap()
    {
        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                map[x, y] = Instantiate(prefab, new Vector3(x * 2.5f, y * 2.5f, 0), Quaternion.identity) ;
                map[x, y].transform.parent = tileHolder.transform;
              
            }
        }


    }

    /// <summary>
    /// Function creates the random path on map and write each step
    /// to array 
    /// </summary>
    private void MakePath()
    {
        while (!stuck)
        {
            
             countTurn = 0;
             ChooseNext();

             if(!stuck)
             {
                 path.Add(currentNode[0] + "" + currentNode[1]);
             }
            
        }
    }

    /// <summary>
    /// Method choose a random path from 4 possible paths,
    /// it will also check for not existing paths.
    /// 
    /// </summary>
    /// <returns></returns>
    private void ChooseNext()
    {
        direction rand = (direction) UnityEngine.Random.Range(0,4);
        bool check;

        if(countTurn > 10)
        {
            stuck = true;
        }

        if (!stuck)
        {
            switch (rand)
            {
                case direction.UP:
                    check = CheckPath(currentNode[0], currentNode[1] + 1);

                    if (!check)
                    {
                        if (currentNode[1] != height - 1)
                        {
                            SetDirection(rand, currentNode);
                            currentNode[1] += 1;
                        }
                        else
                        {
                            countTurn++;
                            ChooseNext();
                            break;
                        }
                        break;
                    }
                    else
                    {
                        countTurn++;
                        ChooseNext();
                        break;
                    }


                case direction.RIGHT:
                    check = CheckPath(currentNode[0] + 1, currentNode[1]);

                    if (!check)
                    {
                        if (currentNode[0] != width - 1)
                        {
                            SetDirection(rand, currentNode);
                            currentNode[0] += 1;
                        }
                        else
                        {
                            countTurn++;
                            ChooseNext();
                            break;
                        }
                        break;
                    }
                    else
                    {
                        countTurn++;
                        ChooseNext();
                        break;
                    }

                case direction.DOWN:
                    check = CheckPath(currentNode[0], currentNode[1] - 1);

                    if (!check)
                    {
                        if (currentNode[1] != 0)
                        {
                            SetDirection(rand, currentNode);
                            currentNode[1] -= 1;
                        }
                        else
                        {
                            countTurn++;
                            ChooseNext();
                            break;
                        }
                        break;
                    }
                    else
                    {
                        countTurn++;
                        ChooseNext();
                        break;
                    }

                case direction.LEFT:
                    check = CheckPath(currentNode[0] - 1, currentNode[1]);

                    if (!check)
                    {
                        if (currentNode[0] != 0)
                        {
                            SetDirection(rand, currentNode);
                            currentNode[0] -= 1;
                        }
                        else
                        {
                            countTurn++;
                            ChooseNext();
                            break;
                        }
                        break;
                    }
                    else
                    {
                        countTurn++;
                        ChooseNext();
                        break;
                    }
            }
        }

        
    }

    /// <summary>
    /// Method checks if passing cell was already used,
    /// false - not used,
    /// true - used
    /// </summary>
    /// <param name="X"></param>
    /// <param name="Y"></param>
    /// <returns></returns>
    private bool CheckPath(int X, int Y)
    {
        String check = X +""+ Y ;
         

        foreach( String step in path)
        {
            if (check.Equals(step))
                return true;
               
        }
        return false;
    }

    private void Wait12()
    {
        Debug.Log("bob");
    }

    /// <summary>
    /// Sets the first and last tiles
    /// </summary>
    private void SetStartFinish()
    {
        last = path[path.Count - 1];

        Destroy(map[start_point[0], start_point[1]]);
        Destroy(map[last[0] - '0', last[1] - '0']);

        map[0, 0] = Instantiate(start, new Vector3((start_point[0]) * 2.5f, (start_point[1]) * 2.5f, 0), Quaternion.identity);
        map[last[0] - '0', last[1] - '0'] = Instantiate(finish, new Vector3((last[0] - '0') * 2.5f, (last[1] - '0') * 2.5f, 0), Quaternion.identity);
    }

    /// <summary>
    /// Function creates arrow sign in passed direction and cell
    /// </summary>
    /// <param name="dir"></param>
    /// <param name="cell"></param>
    private void SetDirection(direction dir, int[] cell)
    {
        switch(dir)
        {
            case direction.UP:
                Instantiate(path_direction, new Vector3(cell[0] * 2.5f + 1.25f , cell[1] * 2.5f + 2, 0), Quaternion.Euler(new Vector3(0,0,90) ) ).transform.parent = arrowHolder.transform;
                dirMap[ cell[0], cell[1] ] += "" + (int)dir;
                break;

            case direction.RIGHT:
                Instantiate(path_direction, new Vector3(cell[0] * 2.5f + 2, cell[1] * 2.5f + 1.25f, 0), Quaternion.Euler(new Vector3(0, 0, 0) ) ).transform.parent = arrowHolder.transform;
                dirMap[ cell[0], cell[1] ] += "" + (int)dir;
                break;

            case direction.DOWN:
                Instantiate(path_direction, new Vector3(cell[0] * 2.5f + 1.25f, cell[1] * 2.5f + 0.5f, 0), Quaternion.Euler(new Vector3(0, 0, -90) ) ).transform.parent = arrowHolder.transform;
                dirMap[ cell[0], cell[1] ] += "" + (int)dir;
                break;

            case direction.LEFT:
                Instantiate(path_direction, new Vector3(cell[0] * 2.5f + 0.5f, cell[1] * 2.5f + 1.25f, 0), Quaternion.Euler(new Vector3(0, 0, 180) ) ).transform.parent = arrowHolder.transform;
                dirMap[ cell[0], cell[1] ] += "" + (int)dir;
                break;

            default:
                break;
        }
    }

    /// <summary>
    /// Function creates extra arrows to frustrate the player.
    /// It restricts directions to point out of map and to finish point
    /// </summary>
    private void SetExtraDirections()
    {
        List<direction> restricted = new List<direction>();

        int finishX = last[0] - '0';
        int finishY = last[1] - '0';

        // false - direction is OK
        // true  - direction is restricted
        bool directionCheck;

        // counts how many times
        // function failed to make direction
        int countFails;

        for (int x = 0; x < width; x++)
        {
            for (int y = 0; y < height; y++)
            {
                restricted.Clear();
                countFails = 0;
                directionCheck = false;

                // checks for restrictions
                // if edge of map or one cell
                // from finish
                if(x == 0 || x == finishX + 1 || x == finishX)
                {
                    restricted.Add(direction.LEFT);
                }

                if (x == width - 1 || x == finishX - 1 || x == finishX)
                {
                    restricted.Add(direction.RIGHT);
                }

                if (y == 0 || y == finishY + 1 || y == finishY)
                {
                    restricted.Add(direction.DOWN);
                }

                if (y == height - 1 || y == finishY - 1 || y == finishY)
                {
                    restricted.Add(direction.UP);
                }

                if(restricted.Count == 4)
                {
                    continue;
                }

                direction rand = (direction)UnityEngine.Random.Range(0, 4);

                // finds possible direction and rejects restricted directions
                while (true)
                {
                    rand = (direction)UnityEngine.Random.Range(0, 4);

                    foreach (direction rest in restricted)
                    {
                        if (rand == rest)
                        {
                            directionCheck = true;
                        }

                    }

                    if(!directionCheck || countFails > 10)
                    {
                        break;
                    }

                    countFails++;
                }


                if (!directionCheck)
                {
                    int[] cell = { x, y };
                    SetDirection(rand, cell);
                }
            }
        }

        
    }

    /// <summary>
    /// Function delete repeating values from dirMap array
    /// </summary>
    private void SortArray()
    {
        for(int x = 0; x < width; x++ )
        {
            for(int y = 0; y < height; y++)
            {
                if(dirMap[x,y] == null)
                {
                    continue;
                }
                else
                {
                    char[] distinct = dirMap[x, y].Distinct().ToArray();
                    dirMap[x, y] = null;

                    foreach (char c in distinct)
                    {
                        dirMap[x, y] += c;
                    }
                }
                
            }
        }
    }
    

    /// <summary>
    /// Function Restart the script by deleting all gameobjects
    /// created before by this script and resetting the variables
    /// </summary>
    /// <param name="Width"></param>
    /// <param name="Height"></param>
    /// <param name="Start_Point"></param>
    public void RestartScript(int Width, int Height, int[] Start_Point)
    {
        // delete the map
        foreach(GameObject tile in map)
        {
            Destroy(tile);
        }
        map = null;
        //Destroy(start);
        //Destroy(finish);
        GameObject[] arrows = GameObject.FindGameObjectsWithTag("arrow");
        // delete arrows
        foreach (GameObject arrow in arrows)
        {
            GameObject.Destroy(arrow);
        }        
        dirMap = null;
        // delete path
        path.Clear();

        // Resetting the script's variable
        width = Width;
        height = Height;
        start_point = Start_Point;
        stuck = false;
        countTurn = 0;

        Start();

       
    }
}
